#### Projet de Web sémantique M1
But: sémantifer un ensemble de données pour faire des requetes efficaces

#### Contributeurs
- Erwan Boisteau-Desdevices
- Lucas Montagnier
- Tristan Rame

Commande pour créer le RDF:

```tarql-1.2/bin/tarql ./mappingWWpollution.rq > res.ttl```

Commande pour lancer un serveur fuseki:

```./fuseki-server --mem /projet```

##### Liste des valeurs possibles des catégories:
- Good
- Moderate
- Unhealthy_for_Sensitive_Groups
- Unhealthy
- Very_Unhealthy
- Hazardous
